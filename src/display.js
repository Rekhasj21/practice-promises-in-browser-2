const displayContainer = document.getElementById("displayContainer")

export function createUserTodoElement(userTodos) {

    const displayList = document.createElement("div")
    displayList.classList.add('display-list')
    displayContainer.appendChild(displayList)

    let listElement = ''
    userTodos.todos.forEach(todo => {
        listElement += `<div class='name-container'>
         <input type='checkbox' class='label'/>
         <h1 class='user-name'>  @${userTodos.name}</h1>
         <p class="todo-list">${todo.title}</p>
         </div>`
    });
    displayList.innerHTML = listElement
}
